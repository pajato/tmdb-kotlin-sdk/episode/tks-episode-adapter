package com.pajato.tks.episode.adapter

import com.pajato.tks.common.adapter.Strategy
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.EpisodeKey
import com.pajato.tks.episode.core.Episode
import com.pajato.tks.episode.core.EpisodeRepo

/**
 * TmdbEpisodeRepo is an object that implements the EpisodeRepo interface to interact with
 * the TMDb (The Movie Database) API to fetch episode data.
 *
 * This class uses TmdbFetcher to perform the actual network operations and handling.
 */
public object TmdbEpisodeRepo : EpisodeRepo {

    /**
     * Fetches a specific episode from the TV show based on the provided EpisodeKey.
     *
     * @param key The unique identifier for the episode which includes TV show ID, season number, and episode number.
     * @return The fetched Episode object containing detailed information about the episode.
     */
    override suspend fun getEpisode(key: EpisodeKey): Episode {
        val path = "tv/${key.id}/season/${key.season}/episode/${key.episode}"
        val extraParams: List<String> = listOf()
        val serializer: Strategy<Episode> = Episode.serializer()
        return TmdbFetcher.fetch(path, extraParams, key, serializer, Episode())
    }
}

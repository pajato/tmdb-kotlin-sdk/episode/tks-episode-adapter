package com.pajato.tks.episode.adapter

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.adapter.TmdbApiService.TMDB_BASE_API3_URL
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.EpisodeKey
import com.pajato.tks.common.core.InjectError
import com.pajato.tks.common.core.jsonFormat
import com.pajato.tks.episode.adapter.TmdbEpisodeRepo.getEpisode
import com.pajato.tks.episode.core.Episode
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

class EpisodeRepoUnitTest : ReportingTestProfiler() {
    private val apiKey = "8G9B0WX31T2U7P0Q"
    private val id = 424 // Torchwood
    private val key: EpisodeKey = EpisodeKey(id, 1, 3)
    private val url = "$TMDB_BASE_API3_URL/tv/$id/season/1/episode/3?api_key=$apiKey"
    private val resourceName = "torchwood_episode_1_3.json"
    private val urlConverterMap: Map<String, String> = mapOf(url to resourceName)

    private lateinit var errorMessage: String
    private lateinit var errorExc: String
    private lateinit var errorExcMessage: String

    @BeforeTest fun setUp() {
        TmdbFetcher.inject(::fetch, ::handleError)
        TmdbFetcher.inject(apiKey)
        errorMessage = ""
        errorExc = ""
        errorExcMessage = ""
    }

    @Test fun `When accessing an episode, verify behavior`() {
        runBlocking {
            val expected = getEpisode(key)
            val episode: Episode = jsonFormat.decodeFromString(getJson(resourceName))
            assertEquals(episode.id, expected.id)
        }
    }

    @Test fun `When accessing a non-cached episode, verify behavior`() {
        fun getExpectedEpisode(): Episode = jsonFormat.decodeFromString(getJson(resourceName))
        runBlocking { assertEquals(expected = getExpectedEpisode().id, actual = getEpisode(key).id) }
    }

    @Test fun `When simulating a Episode fetch without injection, verify behavior`() {
        val expected = "No api key has been injected!: No repo implementation has been injected!"
        TmdbFetcher.reset()
        runBlocking { assertFailsWith<InjectError> { getEpisode(key) }.also { assertEquals(expected, it.message) } }
    }

    private fun fetch(key: String): String {
        val name = urlConverterMap[key] ?: fail("Key $key is not mapped to a test resource name!")
        return getJson(name).ifEmpty { throw IllegalStateException("No JSON available!") }
    }

    private fun getJson(name: String): String = javaClass.classLoader.getResource(name)?.readText() ?: ""

    private fun handleError(message: String, exc: Exception?) {
        errorMessage = message
        errorExc = if (exc == null) "null" else exc.javaClass.name
        errorExcMessage = if (exc == null) "null" else exc.message ?: "null"
    }
}
